import * as prompt from "prompt-sync";
const prompter = prompt({sigint:true});


console.log('Number guessing game');


let minNum : number = parseInt(prompter('enter mininum number : '));
let maxNum : number = parseInt(prompter('enter maximum number : '));


 function generateRandom(min:number,max:number) {

        // find diff
        let difference:number = max - min;
        // console.log(difference);
        // generate random number 
        let random:number = Math.random();
        // console.log(random);
    
        // multiply with difference 
        random = Math.floor( random * difference);
        // console.log(random);
    
        // add with min value 
        random = random + min;
        // console.log(random);
        return random;
    }
    
let randomNumber:number = generateRandom(minNum,maxNum);
// console.log(randomNumber);
let num : number= 0;
    
while (num!=1)
    {
        let guess: number = parseInt(prompter('guess a number:'));
        if (guess == randomNumber)
        {
            console.log('Congratulations !!');
            num = 1;
        }else if (guess > randomNumber)
        {
            console.log('Try Again! You guessed too high');
        }
        else{
            console.log('Try Again! You guessed too small');
        }
    }