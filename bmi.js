"use strict";
exports.__esModule = true;
var prompt = require("prompt-sync");
var prompter = prompt({ sigint: true });
console.log('BMI calculator');
var weight = prompter('enter your weight (in kg):');
var height = prompter('enter your height (in meter):');
var name = prompter('enter your name:');
var bmi = weight / (height * height);
if (bmi < 18.5 && bmi > 0) {
    console.log("".concat(name, " you are Underweight"));
}
else if (bmi >= 18.5 && bmi < 25) {
    console.log("".concat(name, " you are normal"));
}
else if (bmi >= 25 && bmi < 30) {
    console.log("".concat(name, " you are OverWeight"));
}
else if (bmi < 1) {
    console.log('enter valid input');
}
else {
    console.log("".concat(name, " you are obesity"));
}
