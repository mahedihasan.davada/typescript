"use strict";
exports.__esModule = true;
var prompt = require("prompt-sync");
var prompter = prompt({ sigint: true });
console.log('ROCK PAPER SCISSOR');
console.log("let's start");
var attempt = 0;
var p1 = 0;
var p2 = 0;
var play = function () {
    console.log('1 for play');
    console.log('0 for exit');
    var num = parseInt(prompter('enter input:'));
    if (num != 0 && num != 1) {
        console.log('enter valid input');
        play();
    }
    return num;
};
var num = play();
while (num != 0) {
    attempt++;
    var play1 = prompter('player 1 : ').toLowerCase();
    var play2 = prompter('player 2 : ').toLowerCase();
    if (play1 == 'rock' && play2 == 'paper' || play1 == 'paper' && play2 == 'scissor' || play1 == 'scissor' && play2 == 'rock') {
        console.log('player 2 winner !!!');
        num = play();
        p2++;
    }
    else if (play1 == 'rock' && play2 == 'scissor' || play1 == 'paper' && play2 == 'rock' || play1 == 'scissor' && play2 == 'paper') {
        console.log('player 1 winner !!!');
        num = play();
        p1++;
    }
    else if (play1 == 'rock' && play2 == 'rock' || play1 == 'paper' && play2 == 'paper' || play1 == 'scissor' && play2 == 'scissor') {
        console.log('draw');
        num = play();
    }
    else {
        console.log('enter valid input');
        num = play();
    }
}
console.log("total attempt : ".concat(attempt));
console.log("player 1 win ".concat(p1, " times"));
console.log("player 2 win ".concat(p2, " times"));
