"use strict";
exports.__esModule = true;
var prompt = require("prompt-sync");
var prompter = prompt({ sigint: true });
console.log('welcome to recipe app');
var recipe = [];
var start = function () {
    console.log('1 for Add');
    console.log('2 for Update');
    console.log('3 for delete');
    console.log('4 for view');
    console.log('5 for exit');
    var input = parseInt(prompter('enter input : '));
    if (input <= 0 || input > 5) {
        console.log('enter valid input');
        start();
    }
    return input;
};
var input = start();
console.log(input);
// const addRecipe = ()
while (input != 5) {
    if (input == 1) {
        // add
        var name_1 = prompter('enter recipe name : ');
        var type = prompter('enter recipe type : ');
        var desc = prompter('enter recipe description : ');
        recipe.push({ name: name_1, type: type, desc: desc });
        input = start();
    }
    else if (input == 2) {
        // update
        var ind = parseInt(prompter('enter id : '));
        var name_2 = prompter('enter recipe name : ');
        var type = prompter('enter recipe type : ');
        var desc = prompter('enter recipe description : ');
        recipe[--ind] = [{ name: name_2, type: type, desc: desc }];
        input = start();
    }
    else if (input == 3) {
        //delete
        var ind = parseInt(prompter('enter id : '));
        recipe.splice(--ind, 1);
        input = start();
    }
    else if (input == 4) {
        // view
        recipe.forEach(function (recipe, index) {
            console.log(++index, recipe);
        });
        input = start();
    }
}
