import * as prompt from "prompt-sync";
const prompter = prompt({sigint:true});

console.log('welcome to recipe app');

let recipe : any[] =[];
const start = ()=>{
    console.log('1 for Add');
    console.log('2 for Update');
    console.log('3 for delete');
    console.log('4 for view');
    console.log('5 for exit');
    let input:number = parseInt(prompter('enter input : '));
    if (input <= 0 || input > 5)
    {
        console.log('enter valid input');
        start();
    }
 
    return input;
}

let input:number =start();
console.log(input);

// const addRecipe = ()


while(input != 5)
{
    if(input == 1){ 
        // add
        let name : string = prompter('enter recipe name : ');
        let type : string = prompter('enter recipe type : ');
        let desc : string = prompter('enter recipe description : ');
        recipe.push({name,type,desc});   
        input = start();
    }
    else if(input == 2){
          // update
        let ind:number =  parseInt(prompter('enter id : '));
        let name : string = prompter('enter recipe name : ');
        let type : string = prompter('enter recipe type : ');
        let desc : string = prompter('enter recipe description : ');
        recipe[--ind] = {name,type,desc};
        input = start();
    }else if(input == 3){
        //delete
        let ind:number =  parseInt(prompter('enter id : '));
        recipe.splice(--ind,1);
        input = start();
    }else if(input == 4){
        // view
        recipe.forEach((recipe,index)=>{
            console.log(++index,recipe);
        });
        input = start();
    }
}