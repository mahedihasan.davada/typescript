"use strict";
exports.__esModule = true;
var prompt = require("prompt-sync");
var prompter = prompt({ sigint: true });
console.log('Number guessing game');
var minNum = parseInt(prompter('enter mininum number : '));
var maxNum = parseInt(prompter('enter maximum number : '));
function generateRandom(min, max) {
    // find diff
    var difference = max - min;
    // console.log(difference);
    // generate random number 
    var random = Math.random();
    // console.log(random);
    // multiply with difference 
    random = Math.floor(random * difference);
    // console.log(random);
    // add with min value 
    random = random + min;
    // console.log(random);
    return random;
}
var randomNumber = generateRandom(minNum, maxNum);
// console.log(randomNumber);
var num = 0;
while (num != 1) {
    var guess = parseInt(prompter('guess a number:'));
    if (guess == randomNumber) {
        console.log('Congratulations !!');
        num = 1;
    }
    else if (guess > randomNumber) {
        console.log('Try Again! You guessed too high');
    }
    else {
        console.log('Try Again! You guessed too small');
    }
}
