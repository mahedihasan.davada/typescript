import * as prompt from "prompt-sync";
const prompter = prompt({sigint:true});


console.log('ROCK PAPER SCISSOR');
console.log("let's start");

let attempt :number =0;
let p1:number=0;
let p2:number=0;
let play = ()=>{
    console.log('1 for play');
    console.log('0 for exit');
    let num:number = parseInt(prompter('enter input:'));
    if (num != 0 && num !=1)
    {
        console.log('enter valid input');
        play();        
    }
    return num;
}

let num:number = play();
while (num != 0)
{   
    attempt++;
    let play1:string = prompter('player 1 : ').toLowerCase();
    let play2:string = prompter('player 2 : ').toLowerCase();

    if (play1 == 'rock' && play2 == 'paper' || play1 == 'paper' && play2 == 'scissor' || play1 ==           'scissor' && play2 == 'rock')
    {
        console.log('player 2 winner !!!');
        num = play();
        p2++;
    }
    else if (play1 == 'rock' && play2 == 'scissor' || play1 == 'paper' && play2 == 'rock' || play1 == 'scissor' && play2 == 'paper')
    {
        console.log('player 1 winner !!!');
        num = play();
        p1++;
    }else if (play1 == 'rock' && play2 == 'rock' || play1 == 'paper' && play2 == 'paper' || play1 == 'scissor' && play2 == 'scissor')
    {
      console.log('draw');
      num = play(); 
    }
    else{
        console.log('enter valid input');
        num = play();
    }
}


console.log(`total attempt : ${attempt}`);
console.log(`player 1 win ${p1} times`);
console.log(`player 2 win ${p2} times`);




